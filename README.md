# Overview

# Table of Contents
[[_TOC_]]

 **About**
 --
👋 Hi, I'm a:
- :penguin: *nix enthusiast <br> 
- :computer: Infosec practitioner <br> 
- :book: Lifelong learner

**Languages**
--
:seedling: I’m currently learning
- :space_invader: Haskell
- :spider_web: HTML
- :coffee: Java
- :last_quarter_moon_with_face: Lua
- :snake: Python
- :shell: Bash scripting
- :crab: Rust

**Contact**
--
:mailbox: Email: shashiduth.takoor@gmail.com
